/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitrojkatne;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Marek
 */
public class SlowaForm extends javax.swing.JFrame {

    /**
     * Creates new form SlowaForm
     */
    public SlowaForm() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jFileChooser2 = new javax.swing.JFileChooser();
        jFileChooser3 = new javax.swing.JFileChooser();
        jFileChooser4 = new javax.swing.JFileChooser();
        jLabel1 = new javax.swing.JLabel();
        fileButton = new javax.swing.JButton();
        wynikLabel = new javax.swing.JLabel();
        sciezkaField = new javax.swing.JTextField();
        startButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Wybierz ścieżkę do pliku:");

        fileButton.setText("Przegladaj");
        fileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileButtonActionPerformed(evt);
            }
        });

        wynikLabel.setFont(new java.awt.Font("Trajan Pro 3", 0, 14)); // NOI18N
        wynikLabel.setText("Wynik to:");

        sciezkaField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sciezkaFieldActionPerformed(evt);
            }
        });

        startButton.setText("Start");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(sciezkaField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fileButton)))
                .addGap(78, 78, 78))
            .addGroup(layout.createSequentialGroup()
                .addGap(307, 307, 307)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(wynikLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startButton))
                .addContainerGap(119, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sciezkaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fileButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(startButton)
                .addGap(18, 18, 18)
                .addComponent(wynikLabel)
                .addContainerGap(34, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sciezkaFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sciezkaFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_sciezkaFieldActionPerformed

    private void fileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileButtonActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        File plik = chooser.getSelectedFile();
        String nazwapliku = plik.getAbsolutePath();
        sciezkaField.setText(nazwapliku);
    }//GEN-LAST:event_fileButtonActionPerformed

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        SlowaForm suma = new SlowaForm();
        String sciezka = sciezkaField.getText();
        String nowaSciezka = sciezka.replace("\\", "\\\\");
        String input;

        try {
            File plik = new File(nowaSciezka);
            Scanner odczyt = new Scanner(plik);
            input = odczyt.nextLine();

        } catch (Exception e) {
            System.out.println("Blad " + sciezkaField.getText());
            JOptionPane.showMessageDialog(this, "Zła ścieżka",
                    "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        List<String> listaSlow = suma.podzielSlowa(input);
        List<Integer> listaSum = suma.policzSume(listaSlow);
        int wynik = suma.policzSlowa(listaSum);

        this.wynikLabel.setText("Wynik to: " + wynik);

    }//GEN-LAST:event_startButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SlowaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SlowaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SlowaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SlowaForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SlowaForm().setVisible(true);
            }
        });
    }

    //Funkcja zwraca n-ta liczbe trojkatny
    private int ntyWyraz(int n) {
        int a = (n + 1) * n / 2;
        return a;
    }

    //Funkcja zwraca liste liczb trojkatnych mniejszyh lub rownych zadanemu maksowi
    private List<Integer> znajdzWyrazy(int maks) {
        int n = 1;
        int wyraz = 0;
        List<Integer> wyniki = new ArrayList<Integer>();

        SlowaForm test1 = new SlowaForm();

        while (wyraz <= maks) {
            wyraz = test1.ntyWyraz(n);

            if (wyraz <= maks) {
                wyniki.add(wyraz);
            }
            n++;
        }
        return wyniki;
    }

    //Funkcja najpierw dzieli input na poszczegolne slowa i oczyscza z niechcianych znakow (" i ,) a nastepnie rozdziela je do listy
    private List<String> podzielSlowa(String input) {
        String pattern = "[\"]|[,]";
        List<String> slowa = new ArrayList<String>();
        StringTokenizer token = new StringTokenizer(input, pattern);

        int i = 0;
        while (i < 2000) {
            if (token.hasMoreTokens()) {
                slowa.add(token.nextToken());
            }
            i++;
        }
        return slowa;
    }

    //Funckja konwertuje slowa na poszczegolne znaki, nastepnie zamieia je na 
    //liczbe odpowiadajaca jej pozycji w alfabecie i na koncu sumuje
    private List<Integer> policzSume(List<String> slowa) {
        List<Integer> sumy = new ArrayList<Integer>();
        int suma;
        for (int i = 0; i < slowa.size(); i++) {
            suma = 0;
            for (int j = 0; j < slowa.get(i).length(); j++) {
                suma = suma + slowa.get(i).charAt(j) - 64;
            }
            sumy.add(suma);
        }
        return sumy;
    }

    //Funckja porownuje wszystkie sumy z funkcji policzSume z wartosciami
    //liczb trojkatnych, jesli sa sobie rowne dodaje je do listy i zwraca jej wielkosc
    private int policzSlowa(List<Integer> sumy) {
        SlowaForm test1 = new SlowaForm();
        List<Integer> wyniki = new ArrayList<Integer>();

        int sumyMax = Collections.max(sumy);
        List<Integer> wyrazy = test1.znajdzWyrazy(sumyMax);
        int jmax = wyrazy.size();
        int imax = sumy.size();

        for (int i = 0; i < imax; i++) {

            for (int j = 0; j < jmax; j++) {
                if (sumy.get(i).equals(wyrazy.get(j))) {
                    wyniki.add(sumy.get(i));
                }
            }
        }
        return wyniki.size();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton fileButton;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JFileChooser jFileChooser2;
    private javax.swing.JFileChooser jFileChooser3;
    private javax.swing.JFileChooser jFileChooser4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTextField sciezkaField;
    private javax.swing.JButton startButton;
    private javax.swing.JLabel wynikLabel;
    // End of variables declaration//GEN-END:variables
}
