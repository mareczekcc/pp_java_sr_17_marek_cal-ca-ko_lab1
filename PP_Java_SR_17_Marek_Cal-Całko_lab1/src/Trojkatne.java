
import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import java.lang.ArrayIndexOutOfBoundsException;
import java.util.Collections;

public class Trojkatne {

    public static void main(String[] args) throws FileNotFoundException {

        Trojkatne test = new Trojkatne();
        File plik = new File("slowa.txt");

        Scanner odczyt = new Scanner(plik);

        String input = odczyt.nextLine();

        int wynik;

        wynik = test.policzSlowa(test.policzSume(test.podzielSlowa(input)));
        System.out.println("Slowa trojkatne: " + wynik);

    }

    //Funkcja zwraca n-ta liczbe trojkatny
    private int ntyWyraz(int n) {
        int a = (n + 1) * n / 2;
        return a;
    }

    //Funkcja zwraca liste liczb trojkatnych mniejszyh lub rownych zadanemu maksowi
    private List<Integer> znajdzWyrazy(int maks) {
        int n = 1;
        int wyraz = 0;
        List<Integer> wyniki = new ArrayList<Integer>();

        Trojkatne test1 = new Trojkatne();

        while (wyraz <= maks) {
            wyraz = test1.ntyWyraz(n);

            if (wyraz <= maks) {
                wyniki.add(wyraz);
            }
            n++;
        }
        return wyniki;
    }

    //Funkcja najpierw dzieli input na poszczegolne slowa i oczyscza z niechcianych znakow (" i ,) a nastepnie rozdziela je do listy
    private List<String> podzielSlowa(String input) {
        String pattern = "[\"]|[,]";
        List<String> slowa = new ArrayList<String>();
        StringTokenizer token = new StringTokenizer(input, pattern);

        int i = 0;
        while (i < 2000) {
            if (token.hasMoreTokens()) {
                slowa.add(token.nextToken());
            }
            i++;
        }
        return slowa;
    }

    //Funckja konwertuje slowa na poszczegolne znaki, nastepnie zamieia je na 
    //liczbe odpowiadajaca jej pozycji w alfabecie i na koncu sumuje
    private List<Integer> policzSume(List<String> slowa) {
        List<Integer> sumy = new ArrayList<Integer>();
        int suma;
        for (int i = 0; i < slowa.size(); i++) {
            suma = 0;
            for (int j = 0; j < slowa.get(i).length(); j++) {
                suma = suma + slowa.get(i).charAt(j) - 64;
            }
            sumy.add(suma);
        }
        return sumy;
    }

    //Funckja porownuje wszystkie sumy z funkcji policzSume z wartosciami
    //liczb trojkatnych, jesli sa sobie rowne dodaje je do listy i zwraca jej wielkosc
    private int policzSlowa(List<Integer> sumy) {
        Trojkatne test1 = new Trojkatne();

        List<Integer> wyniki = new ArrayList<Integer>();
        for (int i = 0; i < sumy.size(); i++) {
            for (int j = 0; j < test1.znajdzWyrazy(Collections.max(sumy)).size(); j++) {
                if (sumy.get(i).equals(test1.znajdzWyrazy(Collections.max(sumy)).get(j))) {
                    wyniki.add(sumy.get(i));
                }
            }
        }
        return wyniki.size();
    }

}
