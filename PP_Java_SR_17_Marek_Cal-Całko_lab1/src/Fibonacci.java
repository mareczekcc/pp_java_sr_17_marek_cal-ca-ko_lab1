
import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

	public static void main(String[] args) {
				
		Fibonacci test = new Fibonacci();
                
                int limit = 4000000;
		
		System.out.println("Wynik = " +test.dodajElementy(test.znajdzWyrazy(limit)));
	}
        
        //Funkcja znajduje n-ty wyraz ciagu fibonacciego i go zwraca
	private int ntyWyraz(int n){
		int a,b;
		if(n == 0) return 0;
		a=0; b=1;
		for (int i=0; i<(n-1); i++){
			b+=a;
			a=b-a;
                }
		return b;
	}
	
        //Funkcja znajduje wyrazy ciagu fibonacciego mniejsze niz zadana zmienna
        //maks, nastepnie wszystkie zwraca jako lista
	private List<Integer> znajdzWyrazy(int maks){
		int n=1;
		int wyraz =0;
		List<Integer> wyniki= new ArrayList<Integer>();
		
		Fibonacci test1 = new Fibonacci();
		
		while (wyraz < maks){ 
			wyraz = test1.ntyWyraz(n);
			
			if (wyraz < maks){
				wyniki.add(wyraz);
			}
			n++;
		}
		return wyniki;
	}
	
        //funkcja dodaje parzyste elementy zadanej listy i zwraca wynik 
        //w postaci int
	private int dodajElementy(List<Integer> lista){
	
		int wynik = 0;
		
		while (lista.size()!=0){
		if(lista.get(0)%2==0)
		wynik=wynik+lista.get(0);
		lista.remove(0);
		}
		return wynik;
		
	}
	

}
